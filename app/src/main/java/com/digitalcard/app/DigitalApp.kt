package com.digitalcard.app

import com.digitalcard.app.di.AppModule
import com.digitalcard.app.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import timber.log.Timber

class DigitalApp: DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }

    override fun onCreate() {
        Timber.plant(Timber.DebugTree())
        super.onCreate()
    }
}