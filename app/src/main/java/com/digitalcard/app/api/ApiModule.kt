package com.digitalcard.app.api

import android.content.Context
import com.digitalcard.app.di.ApplicationContext
import com.digitalcard.app.utils.LocationProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class ApiModule {
    @Provides
    @Singleton
    fun apiService(@ApplicationContext ctx: Context, source: ApiSource): ApiService {
        return ApiService(ctx, source)
    }

    @Provides
    @Singleton
    fun locationProvider(@ApplicationContext ctx: Context): LocationProvider {
        return LocationProvider(ctx)
    }
}