package com.digitalcard.app.api

import android.content.Context
import com.digitalcard.app.auth.AuthProvider
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import io.reactivex.Completable
import io.reactivex.Observable

class ApiService(
    val ctx: Context,
    val apiSource: ApiSource
) {

    fun checkUserPhone(phoneNumber: String): Completable {
        return Completable.create { emitter ->
            FirebaseDatabase.getInstance().getReference("users/profiles").orderByChild("phone")
                .startAt(phoneNumber)
                .endAt(phoneNumber).addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {
                        emitter.onError(p0.toException())
                    }

                    override fun onDataChange(data: DataSnapshot) {
                        if (data.exists() && data.childrenCount > 0) {
                            emitter.onError(
                                FirebaseAuthUserCollisionException(
                                    "User exists",
                                    "User exists"
                                )
                            )
                        } else {
                            emitter.onComplete()
                        }
                    }

                })
        }
    }

    fun data(path: String): Observable<DataSnapshot> {
        return Observable.create { emitter ->
            val masters = FirebaseDatabase.getInstance().getReference(path)
            masters.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    emitter.onError(p0.toException())
                    emitter.onComplete()
                }

                override fun onDataChange(p0: DataSnapshot) {
                    emitter.onNext(p0)
                    emitter.onComplete()
                }

            })
        }
    }

    fun checkFacebookId(email: String): Completable {
        if (email.isBlank()) {
            return Completable.complete()
        }
        return Completable.create { emitter ->
            FirebaseDatabase.getInstance().getReference("users").child("profiles")
                .orderByChild("facebookID").equalTo(email)
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {
                        emitter.onError(p0.toException())
                    }

                    override fun onDataChange(data: DataSnapshot) {

                        if (data.exists() && data.childrenCount > 0) {
                            if (data.children.any { child ->
                                    val mode =
                                        child.child("mode").getValue(String::class.java) ?: ""
                                    AuthProvider.USER_MODE_CLIENT == mode
                                }) {
                                emitter.onError(
                                    FirebaseAuthUserCollisionException(
                                        "User exists",
                                        "User exists"
                                    )
                                )
                            } else {
                                emitter.onComplete()
                            }
                        } else {
                            emitter.onComplete()
                        }
                    }

                })
        }
    }
}