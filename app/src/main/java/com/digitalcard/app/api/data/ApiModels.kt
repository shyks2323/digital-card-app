package com.digitalcard.app.api.data

import android.os.Parcelable
import com.digitalcard.app.auth.UserProfile
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AddressWrapper(val longitude: Double, val latitude: Double, val title: String) : Parcelable

@Parcelize
data class ClientProfile(
    val id: String,
    val photo: String,
    val firstName: String,
    val lastName: String,
    val phoneNumber: String,
    val email: String,
    val facebookId: String,
    val twitterId: String,
    val instagramId: String,
    val whatsappId: String,
    val skype: String,
    val location: LatLng
) : Parcelable

@Parcelize
data class SocialMediaRow(
    val imageIconId: Int,
    val socialMediaName: String,
    val enabled: Boolean
) : Parcelable

sealed class AddContactEvent {
    data class AddContactButtonPressed (val userProfile: UserProfile?): AddContactEvent()
    object CancelButtonPressed: AddContactEvent()

}