package com.digitalcard.app.auth

import android.content.Context
import com.digitalcard.app.api.ApiService
import com.digitalcard.app.di.ApplicationContext
import com.digitalcard.app.utils.LocationProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AuthModule {
    @Provides
    @Singleton
    fun authProvider(
        @ApplicationContext ctx: Context, api: ApiService,
        locationProvider: LocationProvider
    ): AuthProvider {
        return AuthProvider(ctx, api, locationProvider)
    }
}