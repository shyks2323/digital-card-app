package com.digitalcard.app.auth

import android.content.Context
import android.os.Parcelable
import com.digitalcard.app.api.ApiService
import com.digitalcard.app.ui.signin.UserModeWrongError
import com.digitalcard.app.utils.LocationProvider
import com.facebook.CallbackManager
import com.google.firebase.FirebaseException
import com.google.firebase.auth.*
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.parcel.Parcelize
import timber.log.Timber
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class AuthProvider(ctx: Context, val api: ApiService, val locationProvider: LocationProvider) {
    companion object {
        const val USER_MODE_CLIENT = "clients"
    }

    val clbManager = CallbackManager.Factory.create()!!

    var profile = UserProfile(mode = USER_MODE_CLIENT)

    var currentUser = UserHolder(null)
    val userObservable = BehaviorSubject.create<UserHolder>()

    fun signIn(credentials: AuthCredential): Observable<FirebaseUser> {
        return Observable.create { emitter ->
            FirebaseAuth.getInstance().signInWithCredential(credentials).addOnCompleteListener {
                if (it.isSuccessful) {
                    if (!emitter.isDisposed) {
                        this.currentUser = UserHolder(it.result?.user)
                        userObservable.onNext(this.currentUser)
                        emitter.onNext(it.result?.user!!)
                        emitter.onComplete()
                    }
                } else {
                    if (!emitter.isDisposed) {
                        emitter.onError(it.exception!!)
                        emitter.onComplete()
                    }
                }
            }
        }
    }

    fun listenForUpdates(){

    }

    fun loadCurrentUser(): Single<UserHolder> {
        return Single.create { emitter ->
            val firebaseUser = FirebaseAuth.getInstance().currentUser
            if (firebaseUser == null) {
                profile = UserProfile("", "", "", "", USER_MODE_CLIENT, "", "", "")

                this.currentUser = UserHolder(null)
                userObservable.onNext(this.currentUser)
                emitter.onSuccess(this.currentUser)

            } else {
                this.currentUser = UserHolder(firebaseUser)

                FirebaseDatabase.getInstance().getReference("users/profiles/${firebaseUser.uid}")
                    .addListenerForSingleValueEvent(object : ValueEventListener {
                        override fun onCancelled(p0: DatabaseError) {
                            emitter.onSuccess(currentUser)
                        }

                        override fun onDataChange(snapshot: DataSnapshot) {
                            handleProfileData(snapshot)

                            userObservable.onNext(this@AuthProvider.currentUser)
                            emitter.onSuccess(currentUser)
                        }

                    })
            }
        }
    }

    fun listenForNewUpdates(): Single<UserHolder> {
        return Single.create { emitter ->
            val firebaseUser = FirebaseAuth.getInstance().currentUser
            if (firebaseUser == null) {
                profile = UserProfile("", "", "", "", USER_MODE_CLIENT, "", "", "")

                this.currentUser = UserHolder(null)
                userObservable.onNext(this.currentUser)
                emitter.onSuccess(this.currentUser)

            } else {
                this.currentUser = UserHolder(firebaseUser)

                FirebaseDatabase.getInstance().getReference("users/profiles/${firebaseUser.uid}")
                    .addValueEventListener(object : ValueEventListener {
                        override fun onCancelled(p0: DatabaseError) {
                            emitter.onSuccess(currentUser)
                        }

                        override fun onDataChange(snapshot: DataSnapshot) {
                            handleProfileData(snapshot)

                            userObservable.onNext(this@AuthProvider.currentUser)
                            emitter.onSuccess(currentUser)
                        }

                    })
            }
        }
    }

    private fun handleProfileData(snapshot: DataSnapshot) {
        profile.firstName = snapshot.child("firstName").getValue(String::class.java) ?: ""
        profile.lastName = snapshot.child("lastName").getValue(String::class.java) ?: ""
        profile.photo = snapshot.child("photo").getValue(String::class.java) ?: ""
        profile.mode = snapshot.child("mode").getValue(String::class.java) ?: USER_MODE_CLIENT
        profile.facebookId = snapshot.child("facebookId").getValue(String::class.java) ?: ""
        profile.email = snapshot.child("email").getValue(String::class.java) ?: ""
        profile.twitterId = snapshot.child("twitterId").getValue(String::class.java) ?: ""
        profile.whatsappId = snapshot.child("whatsappId").getValue(String::class.java) ?: ""
        profile.instagramId = snapshot.child("instagramId").getValue(String::class.java) ?: ""
        profile.id = firebaseUid()
        profile.phoneNumber = snapshot.child("phone").getValue(String::class.java) ?: ""


        val savedProfiles = snapshot.child("savedProfiles").children.mapNotNull {
            try {
                val profileId = it.child("id").getValue(String::class.java) ?: ""
                val firstName = it.child("firstName").getValue(String::class.java) ?: ""
                val photo = it.child("photo").getValue(String::class.java) ?: ""
                val phoneNumber = it.child("phone").getValue(String::class.java) ?: ""
                val email = it.child("email").getValue(String::class.java) ?: ""
                val facebookId = it.child("facebookId").getValue(String::class.java) ?: ""
                val whatsappId = it.child("whatsappId").getValue(String::class.java) ?: ""
                val twitterId = it.child("twitterId").getValue(String::class.java) ?: ""
                val instagramId = it.child("instagramId").getValue(String::class.java) ?: ""

                UserProfile(
                    profileId, firstName, photo = photo, phoneNumber = phoneNumber,
                    email = email, facebookId = facebookId, whatsappId = whatsappId,
                    twitterId = twitterId, instagramId = instagramId
                )
            } catch (ex: Throwable) {
                Timber.e(ex)
                null
            }
        }.toMutableList()

        profile.savedProfiles = savedProfiles
    }

    fun firebaseUid() = FirebaseAuth.getInstance().currentUser?.uid ?: ""

    fun signUp(authCredential: AuthCredential): Observable<FirebaseUser> {
        return Observable.create { emitter ->
            FirebaseAuth.getInstance().signInWithCredential(authCredential).addOnCompleteListener {
                if (it.isSuccessful) {
                    if (!emitter.isDisposed) {
                        this.currentUser = UserHolder(it.result?.user)
                        userObservable.onNext(this.currentUser)
                        emitter.onNext(it.result?.user!!)
                        emitter.onComplete()
                    }
                } else {
                    if (!emitter.isDisposed) {
                        emitter.onError(it.exception!!)
                        emitter.onComplete()
                    }
                }
            }
        }
    }

    fun createProfile(): Completable {
        return api.data("users/profiles/${currentUser.uid()}").flatMapCompletable {
            val remoteMode = it.child("mode").getValue(String::class.java) ?: ""
            if (remoteMode.isEmpty() || (profile.mode == remoteMode)) {
                    createProfileClient()
            } else {
                Completable.error(UserModeWrongError())
            }
        }
    }


    fun saveGmailAccountId(gmailId: String): Completable{
        profile.email = gmailId
        return Completable.create {emitter ->
            val uid = firebaseUid()
            val users = FirebaseDatabase.getInstance().getReference("users")
            val map = mapOf<String, Any>(
                "profiles/$uid/email" to profile.email
            )
            users.updateChildren(map) { error, _ ->
                if (error == null) {
                    emitter.onComplete()
                } else {
                    emitter.onError(error.toException())
                }
            }

        }
    }

    fun saveFacebookAccountId(): Completable{
        return Completable.create {emitter ->
            val uid = firebaseUid()
            val users = FirebaseDatabase.getInstance().getReference("users")
            val map = mapOf<String, Any>(
                "profiles/$uid/facebookId" to profile.facebookId,
                "profiles/$uid/photo" to profile.photo,
                "profiles/$uid/firstName" to profile.firstName
            )
            users.updateChildren(map) { error, _ ->
                if (error == null) {
                    emitter.onComplete()
                } else {
                    emitter.onError(error.toException())
                }
            }

        }
    }

    private fun createProfileClient(): Completable {
        return Completable.create { emitter ->
//            FirebaseAuth.getInstance().currentUser?.updateEmail(profile.email)?.addOnSuccessListener {
//                FirebaseAuth.getInstance().currentUser?.sendEmailVerification()
//            }
            val uid = firebaseUid()
            val users = FirebaseDatabase.getInstance().getReference("users")
            val lastKnown = locationProvider.lastKnown
            val map = mapOf(
                "profiles/$uid/firstName" to profile.firstName,
                "profiles/$uid/lastName" to profile.lastName,
                "profiles/$uid/email" to profile.email,
                "profiles/$uid/photo" to profile.photo,
                "profiles/$uid/facebookId" to profile.facebookId,
                "profiles/$uid/mode" to profile.mode,
                "profiles/$uid/phone" to profile.phoneNumber,
                "profiles/$uid/location/longitude" to lastKnown.longitude,
                "profiles/$uid/location/latitude" to lastKnown.latitude
            )
            users.updateChildren(map) { error, _ ->
                if (error == null) {
                    emitter.onComplete()
                } else {
                    emitter.onError(error.toException())
                }
            }
        }
    }

    fun saveScannedProfile(userProfile: UserProfile): Completable {
        return Completable.create { emitter ->
            val users = FirebaseDatabase.getInstance().getReference("users/profiles/${profile.id}/savedProfiles")
            val savedProfileId = users.push().key
            val map = mapOf(
                "$savedProfileId/firstName" to userProfile.firstName,
                "$savedProfileId/lastName" to userProfile.lastName,
                "$savedProfileId/email" to userProfile.email,
                "$savedProfileId/photo" to userProfile.photo,
                "$savedProfileId/facebookId" to userProfile.facebookId,
                "$savedProfileId/whatsappId" to userProfile.whatsappId,
                "$savedProfileId/twitterId" to userProfile.twitterId,
                "$savedProfileId/instagramId" to userProfile.instagramId,
                "$savedProfileId/mode" to userProfile.mode,
                "$savedProfileId/phone" to userProfile.phoneNumber
            )

            users.updateChildren(map) { error, _ ->
                if (error == null) {
                    emitter.onComplete()
                } else {
                    emitter.onError(error.toException())
                }
            }
        }
    }


    fun loginPhone(number: String): Observable<Any> {
        return Observable.create { emitter ->
            PhoneAuthProvider.getInstance()
                .verifyPhoneNumber(number, 30, TimeUnit.SECONDS, Executors.newCachedThreadPool(), object :
                    PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                    override fun onVerificationCompleted(p0: PhoneAuthCredential) {
                        Timber.d("onVerificationCompleted $p0")
                        if (!emitter.isDisposed) {
                            emitter.onNext(p0)
                        }
                    }

                    override fun onVerificationFailed(p0: FirebaseException) {
                        Timber.e(p0, "onVerificationFailed")
                        if (!emitter.isDisposed) {
                            emitter.onError(p0)
                        }

                    }

//                    override fun onCodeAutoRetrievalTimeOut(p0: String) {
//                        Timber.d("onCodeAutoRetrievalTimeOut $p0")
//                        super.onCodeAutoRetrievalTimeOut(p0)
//                        if (!emitter.isDisposed) {
//                            emitter.onError(CodeAutoRetrievalTimeOut())
//                        }
//                    }

                    override fun onCodeSent(p0: String, p1: PhoneAuthProvider.ForceResendingToken) {
                        Timber.d("onCodeSent $p0 $p1")
                        super.onCodeSent(p0, p1)
                        if (!emitter.isDisposed) {
                            emitter.onNext(CodeSent(p0, p1))
                        }
                    }

                })

        }
    }
}

class CodeAutoRetrievalTimeOut : Exception()
@Parcelize
data class UserProfile(
    var id: String? = "",
    var firstName: String = "",
    var lastName: String = "",
    var photo: String = "",
    var mode: String = "",
    var email: String = "",
    var facebookId: String = "",
    var facebookProfileName: String = "",
    var twitterId: String = "",
    var whatsappId: String = "",
    var instagramId: String = "",
    var phoneNumber: String = "",
    var savedProfiles: List<UserProfile>? = null
) : Parcelable

data class CodeSent(val token: String, val resendToken: PhoneAuthProvider.ForceResendingToken)

data class UserHolder(val user: FirebaseUser?) {
    fun isAnonymous(): Boolean {
        return user == null
    }

    fun uid(): String {
        return user?.uid ?: ""
    }


}