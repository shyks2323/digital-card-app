package com.digitalcard.app.di

import com.digitalcard.app.ui.HomeNavigation
import com.digitalcard.app.ui.camera.CameraFragment
import com.digitalcard.app.ui.contactslist.AllContactsFragment
import com.digitalcard.app.ui.home.HomeFragment
import com.digitalcard.app.ui.profile.ProfileFragment
import com.digitalcard.app.ui.settings.SettingsFragment
import com.digitalcard.app.ui.signin.LoginFragment
import com.digitalcard.app.ui.signin.SignInActivity
import com.digitalcard.app.ui.signin.VerificationFragment
import com.digitalcard.app.ui.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivitiesModule {
    @ContributesAndroidInjector
    abstract fun splashActivity(): SplashActivity

    @ContributesAndroidInjector
    abstract fun signInActivity(): SignInActivity

    @ContributesAndroidInjector
    abstract fun navigationActivity(): HomeNavigation

    @ContributesAndroidInjector
    abstract fun homeFragment(): HomeFragment

    @ContributesAndroidInjector
    abstract fun loginFragment(): LoginFragment

    @ContributesAndroidInjector
    abstract fun profileFragment(): ProfileFragment

    @ContributesAndroidInjector
    abstract fun cameraFragment(): CameraFragment

    @ContributesAndroidInjector
    abstract fun verificationFragment(): VerificationFragment

    @ContributesAndroidInjector
    abstract fun allContactsFragment(): AllContactsFragment

    @ContributesAndroidInjector
    abstract fun settingsFragment(): SettingsFragment
}