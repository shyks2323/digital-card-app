package com.digitalcard.app.di

import android.app.Application
import android.content.Context
import com.digitalcard.app.api.ApiModule
import com.digitalcard.app.auth.AuthModule
import com.digitalcard.app.utils.RxBus
import com.mymaster.android.di.vm.ViewModelModule
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(
    includes = [
        ViewModelModule::class,
        ActivitiesModule::class,
        AuthModule::class,
        ApiModule::class]
)
class AppModule(val app: Application) {
    @Provides
    internal fun app(): Application {
        return app
    }

    @ApplicationContext
    @Provides
    internal fun appContext(): Context {
        return app
    }

    @Provides
    @Singleton
    internal fun picasso(): Picasso {
        return Picasso.get()
    }

    @Provides
    @Singleton
    internal fun rxBus(): RxBus {
        return RxBus()
    }
}