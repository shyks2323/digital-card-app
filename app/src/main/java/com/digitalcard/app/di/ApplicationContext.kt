package com.digitalcard.app.di

import javax.inject.Qualifier

@Qualifier
@Retention
annotation class ApplicationContext

@Qualifier
@Retention
annotation class WorkerScheduler

@Qualifier
@Retention
annotation class MainScheduler

@Qualifier
@Retention
annotation class SingleScheduler