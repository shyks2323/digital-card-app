package com.digitalcard.app.di

import android.content.Context
import android.graphics.BitmapFactory
import androidx.core.graphics.drawable.RoundedBitmapDrawable
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import com.digitalcard.app.R
import com.digitalcard.app.auth.UserProfile
import com.digitalcard.app.utils.CircleTransform
import com.digitalcard.app.utils.UrlGeneratorHelper
import com.squareup.picasso.Picasso
import com.squareup.picasso.RequestCreator


class PhotoLoader(val ctx: Context, val picasso: Picasso) {
    fun loadProfile(profile: UserProfile): RequestCreator {
        return picasso.load(UrlGeneratorHelper.getUrl(profile.photo)).transform(CircleTransform())
            .error(setCircularImage(ctx, R.drawable.icon_male_ph))
            .placeholder(setCircularImage(ctx, R.drawable.icon_male_ph))
    }

    fun setCircularImage(ctx: Context, id: Int): RoundedBitmapDrawable {
        val res = ctx.resources
        val src = BitmapFactory.decodeResource(res, id)
        val roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(res, src)
        roundedBitmapDrawable.cornerRadius = Math.max(src.width, src.height) / 2.0f
        return roundedBitmapDrawable
    }
}