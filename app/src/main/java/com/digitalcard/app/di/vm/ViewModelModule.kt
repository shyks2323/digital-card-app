package com.mymaster.android.di.vm

import androidx.lifecycle.ViewModel
import com.digitalcard.app.ui.contactslist.AllContactsViewModel
import com.digitalcard.app.ui.home.HomeViewModel
import com.digitalcard.app.ui.profile.ProfileViewModel
import com.digitalcard.app.ui.settings.SettingsViewModel
import com.digitalcard.app.ui.signin.RegisterClientViewModel
import com.digitalcard.app.ui.splash.SplashViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    internal abstract fun bindSplashViewModel(viewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    internal abstract fun bindHomeViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel::class)
    internal abstract fun bindProfileViewModel(viewModel: ProfileViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(RegisterClientViewModel::class)
    internal abstract fun registerClientViewModel(viewModel: RegisterClientViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AllContactsViewModel::class)
    internal abstract fun registerAllContactsViewModel(viewModel: AllContactsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SettingsViewModel::class)
    internal abstract fun registerSettingsViewModel(viewModel: SettingsViewModel): ViewModel


}