package com.digitalcard.app.ui

import android.annotation.SuppressLint
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.disposables.CompositeDisposable


@SuppressLint("Registered")
open class BaseActivity : DaggerAppCompatActivity() {
    val compositeDisposable = CompositeDisposable()
    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}