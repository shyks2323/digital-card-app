package com.digitalcard.app.ui

import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable


open class BaseFragment : DaggerFragment() {
    val compositeDisposable = CompositeDisposable()
    override fun onDestroyView() {
        compositeDisposable.clear()
        super.onDestroyView()
    }
}