package com.digitalcard.app.ui

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber


open class BaseViewModel : ViewModel() {
    val compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        Timber.d("onCleared called for ${javaClass.canonicalName}")
        compositeDisposable.clear()
        super.onCleared()
    }
}