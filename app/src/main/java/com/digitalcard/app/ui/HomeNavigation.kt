package com.digitalcard.app.ui

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.navigation.NavController
import androidx.navigation.NavHost
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.digitalcard.app.R
import com.digitalcard.app.api.ApiService
import com.digitalcard.app.auth.AuthProvider
import com.digitalcard.app.ui.profile.FaceBookAccountAddedEvent
import com.digitalcard.app.ui.profile.ProfileViewModel
import com.digitalcard.app.utils.RxBus
import com.digitalcard.app.vm.HomeNavigationViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.tbruyelle.rxpermissions2.RxPermissions
import kotlinx.android.synthetic.main.activity_home.*
import javax.inject.Inject


class HomeNavigation : BaseActivity(), NavHost {

    override fun getNavController(): NavController {
        return findNavController(R.id.nav_host_fragment)
    }

    @Inject
    lateinit var rxBus: RxBus
    @Inject
    lateinit var apiService: ApiService
    @Inject
    lateinit var authProvider: AuthProvider
    @Inject
    lateinit var homeNavigationVM: HomeNavigationViewModel

    lateinit var rxPermissions: RxPermissions

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        rxPermissions = RxPermissions(this)

        navController.setGraph(R.navigation.nav_graph)
        bottom_nav_view.setupWithNavController(navController)

        bottom_nav_view.selectedItemId = R.id.to_HomeScreen

        bottom_nav_view.setOnNavigationItemSelectedListener { p0 ->
            when(p0.itemId){
                R.id.to_CameraScreen -> {
                    navController.popBackStack()
                    navController.navigate(R.id.action_to_cameraFragment)
                }
                R.id.to_ProfileScreen -> {
                    navController.popBackStack()
                    navController.navigate(R.id.action_to_profileFragment)
                }
                R.id.to_HomeScreen -> {
                    navController.popBackStack()
                    navController.navigate(R.id.action_to_global_home)
                }
                R.id.to_SocialMedia -> {
                    navController.popBackStack()
                    navController.navigate(R.id.action_global_contactDetailsFragment)
                }
                R.id.to_SettingsScreen -> {
                    navController.popBackStack()
                    navController.navigate(R.id.action_global_settingsFragment)
                }
                else -> {
                }
            }
            true
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        rxBus.post(FaceBookAccountAddedEvent(requestCode, resultCode, data))
    }

    override fun onSupportNavigateUp() = findNavController(R.id.nav_host_fragment).navigateUp()
}