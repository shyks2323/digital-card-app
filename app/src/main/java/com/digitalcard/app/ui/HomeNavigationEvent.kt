package com.digitalcard.app.ui

sealed class HomeNavigationEvent {
    object ClientSignInEvent : HomeNavigationEvent()
}