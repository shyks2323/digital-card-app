package com.digitalcard.app.ui.camera

import android.Manifest
import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.budiyev.android.codescanner.CodeScanner
import com.budiyev.android.codescanner.CodeScannerView
import com.budiyev.android.codescanner.DecodeCallback
import com.digitalcard.app.R
import com.digitalcard.app.api.data.AddContactEvent
import com.digitalcard.app.auth.AuthProvider
import com.digitalcard.app.auth.UserProfile
import com.digitalcard.app.ui.BaseFragment
import com.digitalcard.app.utils.VCardPlus
import com.digitalcard.app.utils.createAddContactDialog
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class CameraFragment : BaseFragment(){

    private lateinit var codeScanner: CodeScanner

    @Inject
    lateinit var authProvider: AuthProvider

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_camera, container, false)
    }

    @SuppressLint("CheckResult")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val scannerView = view.findViewById<CodeScannerView>(R.id.scanner_view)
        val activity = requireActivity()
        codeScanner = CodeScanner(activity, scannerView)
        codeScanner.decodeCallback = DecodeCallback {
            val vCardPlus = VCardPlus.Builder().build()
            val result = vCardPlus.parseSchema(it.text)
            val userProfile = result!!.toUserProfile()
            activity.runOnUiThread {
                showAddUserDialog(userProfile)
//                Toast.makeText(activity, it.text, Toast.LENGTH_LONG).show()
            }
        }

        val rxPermissions = RxPermissions(this)
        rxPermissions
            .request(Manifest.permission.CAMERA)
            .subscribe { granted: Boolean ->
                if (granted) {
                    codeScanner.startPreview()
                    // All requested permissions are granted
                } else {
                    // At least one permission is denied
                }
            }

        scannerView.setOnClickListener {
            codeScanner.startPreview()
        }
    }

    private fun showAddUserDialog(userProfile: UserProfile) {
        val dialog = createAddContactDialog(requireActivity(), userProfile) { newValue ->
            when (newValue){
                is AddContactEvent.AddContactButtonPressed -> {
                    compositeDisposable.add(authProvider.saveScannedProfile(newValue.userProfile!!).subscribeOn(Schedulers.newThread()).subscribe {

                    })
                }
            }
        }

        dialog.show()
    }

    override fun onPause() {
        codeScanner.releaseResources()
        super.onPause()
    }
}