package com.digitalcard.app.ui.contactslist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.digitalcard.app.R
import com.digitalcard.app.api.data.ClientProfile
import com.digitalcard.app.auth.AuthProvider
import com.digitalcard.app.auth.UserProfile
import com.digitalcard.app.ui.BaseFragment
import com.digitalcard.app.utils.RxBus
import com.mymaster.android.di.vm.ViewModelFactory
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_all_contacts.*
import timber.log.Timber
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class AllContactsFragment : BaseFragment() {

    private val GMAIL_REQUEST_CODE: Int = 101

    @Inject
    lateinit var vmFactory: ViewModelFactory

    lateinit var vm: AllContactsViewModel

    @Inject
    lateinit var rxBus: RxBus

    @Inject
    lateinit var picasso: Picasso

    @Inject
    lateinit var authProvider: AuthProvider

    private lateinit var allContactsAdapter: AllContactsRecyclerAdapter
    private lateinit var models: ArrayList<ClientProfile>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_all_contacts, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm = ViewModelProviders.of(this, vmFactory)[AllContactsViewModel::class.java]

        fillCurrentUserHolderData(authProvider.profile)

        allContactsAdapter = AllContactsRecyclerAdapter(requireActivity(), picasso, rxBus)
        allProfilesRecyclerView.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        allProfilesRecyclerView.adapter = allContactsAdapter

        vm.load()
    }

    private fun fillCurrentUserHolderData(profile: UserProfile) {
        if (!profile.savedProfiles.isNullOrEmpty()){
            emptyView.visibility = View.GONE
            allProfilesRecyclerView.visibility = View.VISIBLE
            requireActivity().runOnUiThread {
                allContactsAdapter.update(profile.savedProfiles!!)
            }
        } else {
            emptyView.visibility = View.VISIBLE
            allProfilesRecyclerView.visibility = View.GONE
        }

    }
}