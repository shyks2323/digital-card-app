package com.digitalcard.app.ui.contactslist

import android.app.Activity
import android.os.Bundle
import com.digitalcard.app.api.ApiService
import com.digitalcard.app.auth.AuthProvider
import com.digitalcard.app.auth.UserHolder
import com.digitalcard.app.ui.BaseViewModel
import com.digitalcard.app.ui.signin.ProgressCompletedEvent
import com.digitalcard.app.ui.signin.ProgressStartedEvent
import com.digitalcard.app.utils.LocationProvider
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.squareup.picasso.Picasso
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

class AllContactsViewModel @Inject constructor(val authProvider: AuthProvider,
                                           val apiService: ApiService,
                                           val locationProvider: LocationProvider
) : BaseViewModel() {

    val dataObservable = PublishSubject.create<UserHolder>()


    val processObserver = PublishSubject.create<Any>()
    val errorsObserver = PublishSubject.create<Throwable>()
    val credentialsSubject = PublishSubject.create<AuthCredential>()


    fun load() {
        compositeDisposable.add(userHolder().subscribe({
            dataObservable.onNext(it)
        },{
            dataObservable.onError(it)
        }))
//        fillDefaults()
    }

    private fun userHolder(): Observable<UserHolder> {
        return authProvider.loadCurrentUser().toObservable()
    }
}