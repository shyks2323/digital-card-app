package com.digitalcard.app.ui.home

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.digitalcard.app.R
import com.digitalcard.app.auth.AuthProvider
import com.digitalcard.app.ui.BaseFragment
import com.digitalcard.app.utils.RxBus
import com.digitalcard.app.utils.VCardPlus
import com.mymaster.android.di.vm.ViewModelFactory
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_home.*
import net.glxn.qrgen.android.QRCode
import net.glxn.qrgen.core.scheme.VCard
import javax.inject.Inject


class HomeFragment : BaseFragment() {
    @Inject
    lateinit var vmFactory: ViewModelFactory

    lateinit var vm: HomeViewModel

    @Inject
    lateinit var rxBus: RxBus

    @Inject
    lateinit var authProvider: AuthProvider

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm = ViewModelProviders.of(this, vmFactory)[HomeViewModel::class.java]

        // encode contact data as vcard using defaults
        // encode contact data as vcard using defaults

        val profile = authProvider.profile

        val vCardPlusBuilder = VCardPlus.Builder()

        vCardPlusBuilder.name(profile.firstName)

        if (profile.phoneNumber.isNotEmpty()){
            vCardPlusBuilder.phoneNumber(profile.phoneNumber)
        }

        if (profile.photo.isNotEmpty()){
            vCardPlusBuilder.photo(profile.photo)
        }


        if (profile.email.isNotEmpty()){
            vCardPlusBuilder.email(profile.email)
        }

        if (profile.facebookId.isNotEmpty()){
            vCardPlusBuilder.facebook(profile.facebookId)
        }
        if (profile.email.isNotEmpty()){
            vCardPlusBuilder.email(profile.email)
        }

        val result = vCardPlusBuilder.build()

        val myBitmap: Bitmap = QRCode.from(result).withSize(250, 250).bitmap()
        qrcodeImage.setImageBitmap(myBitmap)

        compositeDisposable.add(rxBus.events.observeOn(AndroidSchedulers.mainThread()).subscribe {
        })

        vm.load()

        shareButton.setOnClickListener {
            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, result.toReadableFormatString())
                type = "text/plain"
            }

            val shareIntent = Intent.createChooser(sendIntent, null)
            startActivity(shareIntent)
        }
    }
}