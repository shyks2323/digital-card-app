package com.digitalcard.app.ui.profile

import android.accounts.AccountManager
import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.digitalcard.app.R
import com.digitalcard.app.api.data.ClientProfile
import com.digitalcard.app.api.data.SocialMediaRow
import com.digitalcard.app.auth.AuthProvider
import com.digitalcard.app.ui.BaseFragment
import com.digitalcard.app.ui.signin.ProgressCompletedEvent
import com.digitalcard.app.ui.signin.ProgressStartedEvent
import com.digitalcard.app.utils.RxBus
import com.google.android.gms.common.AccountPicker
import com.google.android.gms.common.AccountPicker.AccountChooserOptions
import com.jakewharton.rxbinding3.view.clicks
import com.mymaster.android.di.vm.ViewModelFactory
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.item_social_media_row.view.*
import timber.log.Timber
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

data class FaceBookAccountAddedEvent (val requestCode: Int = -1, val resultCode: Int = -1, val data: Intent? = null)

class ProfileFragment : BaseFragment() {

    private val GMAIL_REQUEST_CODE: Int = 101

    @Inject
    lateinit var vmFactory: ViewModelFactory

    lateinit var vm: ProfileViewModel

    @Inject
    lateinit var rxBus: RxBus

    @Inject
    lateinit var picasso: Picasso

    @Inject
    lateinit var authProvider: AuthProvider

    private lateinit var socialMediaAdapter: SocialMediaAdapter

    private lateinit var scannedProfilesAdapter: ScannedProfilesRecyclerAdapter
    private lateinit var models: ArrayList<ClientProfile>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm = ViewModelProviders.of(this, vmFactory)[ProfileViewModel::class.java]

        compositeDisposable.add(vm.processObserver
            .observeOn(Schedulers.newThread())
            .subscribeOn(AndroidSchedulers.mainThread())
            .subscribe {
            when (it) {
                is ProgressStartedEvent -> {
                }
                is ProgressCompletedEvent -> {
                    if (authProvider.profile.facebookId.isNotEmpty()){
                        fillFacebookAccountDetails()
                    }
                }
            }
        })

        socialMediaAdapter = SocialMediaAdapter(requireActivity(), picasso, rxBus)
        recyclerView.layoutManager = LinearLayoutManager(requireActivity())
        recyclerView.adapter = socialMediaAdapter

        fillCurrentUserHolderData()

        compositeDisposable.add(vm.dataObservable
            .observeOn(Schedulers.newThread())
            .subscribeOn(AndroidSchedulers.mainThread())
            .doOnError {
            Timber.e("On error: ${it.message}")
        }.throttleWithTimeout(2,
            TimeUnit.SECONDS).subscribe {userHolder ->
            Timber.d("Finish splash")
            if (!userHolder.isAnonymous()) {
                fillCurrentUserHolderData()
        }
        })

        compositeDisposable.add(gmailAccount.clicks().throttleFirst(1, TimeUnit.SECONDS).subscribe ({
            val intent = AccountPicker.newChooseAccountIntent(
                AccountChooserOptions.Builder()
                    .setAllowableAccountsTypes(listOf("com.google"))
                    .build()
            )
            startActivityForResult(intent, GMAIL_REQUEST_CODE)
        }, {
            Timber.e(it)
        }))

        compositeDisposable.add(facebookAccount.clicks().throttleFirst(1, TimeUnit.SECONDS).subscribe ({
            vm.facebookStart(requireActivity(), true)
        }, {
            Timber.e(it)
        }))

        compositeDisposable.add(viewProfiles.clicks().subscribe {
            findNavController().navigate(R.id.action_profileFragment_to_contactDetailsFragment)
        })
        compositeDisposable.add(vm.authProvider.listenForNewUpdates().subscribe ({
            vm.load()
        }, {
            Timber.e(it)
        }))


        compositeDisposable.add(vm.credentialsSubject.subscribe({
            if (it != null){
                fillFacebookAccountDetails()
            }
        }, {
            Timber.e(it)
        }))

        compositeDisposable.add(rxBus.events.observeOn(AndroidSchedulers.mainThread()).subscribe {
            if (it is FaceBookAccountAddedEvent) {
                authProvider.clbManager.onActivityResult(it.requestCode, it.resultCode, it.data)
            }
        })

        scannedProfilesAdapter = ScannedProfilesRecyclerAdapter(requireActivity(), picasso, rxBus)
        scannedProfilesRecyclerView.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.HORIZONTAL, false)
        scannedProfilesRecyclerView.adapter = scannedProfilesAdapter

        if ((authProvider.profile.savedProfiles != null) && authProvider.profile.savedProfiles?.size != 0) {
            scannedProfilesAdapter.update(authProvider.profile.savedProfiles!!)
            scannedProfilesRecyclerView.visibility = View.VISIBLE
            emptyView.visibility = View.GONE
        } else {
            scannedProfilesRecyclerView.visibility = View.GONE
            emptyView.visibility = View.VISIBLE
        }

        if (vm.authProvider.profile.photo.isNotEmpty()){
            Glide.with(requireActivity()).load(vm.authProvider.profile.photo).into(userImage)
        }

        if (vm.authProvider.profile.firstName.isNotEmpty()){
            userName.setText(vm.authProvider.profile.firstName)
        }

    }

    private fun fillFacebookAccountDetails() {

        compositeDisposable.add(vm.saveFacebookAccountId()
            .observeOn(Schedulers.newThread())
            .subscribeOn(AndroidSchedulers.mainThread())
            .doFinally {
                setBackgroundResourceForButton(facebookAccount, R.drawable.ic_facebook)
                vm.load()
            }
            .doOnError {
                Timber.e("On error: ${it.message}")
            }.throttleWithTimeout(2,
                TimeUnit.SECONDS).subscribe {userHolder ->
                Timber.d("Finish splash")
            })

        Glide.with(requireActivity()).load(vm.authProvider.profile.photo).into(userImage)



    }

    private fun fillCurrentUserHolderData() {
        val resultList = mutableListOf<SocialMediaRow>()

        resultList.add(SocialMediaRow(R.drawable.ic_baseline_call_24, authProvider.profile.phoneNumber, true))

        authProvider.profile.apply {
            if (facebookId.isNotEmpty()){
                resultList.add(SocialMediaRow(R.drawable.ic_facebook, facebookId, true))
                setBackgroundResourceForButton(facebookAccount, R.drawable.ic_facebook)
            }
            if (email.isNotEmpty()){
                resultList.add(SocialMediaRow(R.drawable.ic_gmail, email, true))
                setBackgroundResourceForButton(gmailAccount, R.drawable.ic_gmail)
//                gmailAccount.setBackgroundResource(R.drawable.ic_gmail)
            }
            if (twitterId.isNotEmpty()){
                resultList.add(SocialMediaRow(R.drawable.ic_twitter, twitterId, true))
                setBackgroundResourceForButton(twitterAccount, R.drawable.ic_twitter)
            }
            if (whatsappId.isNotEmpty()){
                resultList.add(SocialMediaRow(R.drawable.ic_whatsapp, whatsappId, true))
                setBackgroundResourceForButton(whatsappAccount, R.drawable.ic_whatsapp)
            }
            if (instagramId.isNotEmpty()){
                resultList.add(SocialMediaRow(R.drawable.ic_instagram, instagramId, true))
                setBackgroundResourceForButton(instagramAccount, R.drawable.ic_instagram)
            }
        }
        requireActivity().runOnUiThread {
            socialMediaAdapter.update(resultList)
        }
    }

    private fun setBackgroundResourceForButton(imageview: ImageView, drawableId: Int) {
        requireActivity().runOnUiThread {
            Glide.with(requireActivity()).load(drawableId).into(imageview)
        }
    }

    internal class ProfileVH(val context: Context, itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(
            picasso: Picasso,
            item: SocialMediaRow,
            rxBus: RxBus
        ) {

            try {
                Glide.with(context).load(item.imageIconId).into(itemView.app_icon)
            } catch (e: IllegalStateException){
                e.printStackTrace()
            }

            itemView.phone_number.text = item.socialMediaName
            itemView.app_hide.isSelected = item.enabled

            itemView.setOnClickListener {
                rxBus.post(item)
            }
        }
    }

    internal class SocialMediaAdapter(
        val ctx: Context,
        private val picasso: Picasso,
        private val rxBus: RxBus) :
        RecyclerView.Adapter<ProfileVH>() {
        private var lst  = mutableListOf<SocialMediaRow>()
        private val layoutInflater = LayoutInflater.from(ctx)
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProfileVH {
            return ProfileVH(ctx, layoutInflater.inflate(R.layout.item_social_media_row, parent, false))
        }

        override fun getItemCount(): Int {
            return lst.size
        }

        override fun onBindViewHolder(holder: ProfileVH, position: Int) {
            val item = lst[position]
            holder.bind(picasso, item, rxBus)
        }

        fun update(items: List<SocialMediaRow>) {

            val diffs = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                    return lst[oldItemPosition].socialMediaName == items[newItemPosition].socialMediaName
                }

                override fun getOldListSize(): Int {
                    return lst.size
                }

                override fun getNewListSize(): Int {
                    return items.size
                }

                override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                    return lst[oldItemPosition].socialMediaName == items[newItemPosition].socialMediaName
                }
            })
            lst.clear()
            lst.addAll(items)
            diffs.dispatchUpdatesTo(this)
        }
    }

    @SuppressLint("CheckResult")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        authProvider.clbManager.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GMAIL_REQUEST_CODE && resultCode == RESULT_OK) {
            val accountName = data!!.getStringExtra(AccountManager.KEY_ACCOUNT_NAME)
            Log.e("onActivityResult","accountName=$accountName")
            accountName?.let {
                compositeDisposable.add(vm.saveGmailAccountId(accountName)
                    .observeOn(Schedulers.newThread())
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .doFinally {
                        vm.load()
                    }
                    .doOnError {
                        Timber.e("On error: ${it.message}")
                    }.throttleWithTimeout(2,
                        TimeUnit.SECONDS).subscribe {userHolder ->
                        Timber.d("Finish splash")
                    })
            }
            // Do what you need with email
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}