package com.digitalcard.app.ui.profile

import android.app.Activity
import android.os.Bundle
import com.digitalcard.app.api.ApiService
import com.digitalcard.app.auth.AuthProvider
import com.digitalcard.app.auth.UserHolder
import com.digitalcard.app.ui.BaseViewModel
import com.digitalcard.app.ui.signin.ProgressCompletedEvent
import com.digitalcard.app.ui.signin.ProgressStartedEvent
import com.digitalcard.app.utils.LocationProvider
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.squareup.picasso.Picasso
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

class ProfileViewModel @Inject constructor(val authProvider: AuthProvider,
                                           val apiService: ApiService,
                                           val locationProvider: LocationProvider
) : BaseViewModel() {

    val dataObservable = PublishSubject.create<UserHolder>()


    val processObserver = PublishSubject.create<Any>()
    val errorsObserver = PublishSubject.create<Throwable>()
    val credentialsSubject = PublishSubject.create<AuthCredential>()


    fun load() {
        compositeDisposable.add(userHolder().subscribe({
            dataObservable.onNext(it)
        },{
            dataObservable.onError(it)
        }))
//        fillDefaults()
    }

    private fun userHolder(): Observable<UserHolder> {
        return authProvider.loadCurrentUser().toObservable()
    }

    private fun refreshData() {
    }



    fun saveGmailAccountId(gmailId: String) : Observable<String> {
        return authProvider.saveGmailAccountId(gmailId).toObservable()
    }

    fun saveFacebookAccountId() : Observable<String> {
        return authProvider.saveFacebookAccountId().toObservable()
    }

    class FacebookUserCollisionException:Exception()

    fun facebookStart(fragment: Activity, validate: Boolean) {
        processObserver.onNext(ProgressStartedEvent())
        FacebookSdk.addLoggingBehavior(LoggingBehavior.REQUESTS)
        LoginManager.getInstance().logOut()
        LoginManager.getInstance().registerCallback(authProvider.clbManager, object :
            FacebookCallback<LoginResult> {
            override fun onCancel() {
                Timber.d("onCancel")
            }

            override fun onError(error: FacebookException) {
                Timber.e(error, "onError")
                errorsObserver.onNext(error)
            }

            override fun onSuccess(result: LoginResult) {
                Timber.d("onSuccess")

                val graphrequest =
                    GraphRequest.newMeRequest(result.accessToken) { jsonObj, response ->

                        if (response.error == null){
                            compositeDisposable.add(apiService.checkFacebookId(jsonObj.optString("id")).subscribe({
//                                if (validate) {
//                                    errorsObserver.onNext(FacebookUserNotExistsError())
//                                    processObserver.onNext(ProgressCompletedEvent())
//                                } else {
                                    authProvider.profile.facebookId = jsonObj.optString("name")
                                    if (authProvider.profile.firstName.isBlank()) {
                                        authProvider.profile.firstName = jsonObj.optString("name")
                                    }
//                                    if (authProvider.profile.lastName.isBlank()) {
//                                        authProvider.profile.lastName = jsonObj.optString("last_name")
//                                    }
//                                    if (authProvider.profile.email.isBlank()) {
//                                        authProvider.profile.email = jsonObj.optString("email")
//                                    }
                                    if (authProvider.profile.photo.isBlank()) {
                                        val picture = jsonObj.optJSONObject("picture")
                                        val data = picture?.optJSONObject("data")
                                        authProvider.profile.photo = data?.optString("url") ?: ""
                                    }
                                    credentialsSubject.onNext(FacebookAuthProvider.getCredential(result.accessToken.token))
//                                }
                            }, {
                                if (validate && it is FirebaseAuthUserCollisionException) {
                                    authProvider.profile.facebookId = jsonObj.optString("name")
                                    if (authProvider.profile.facebookProfileName.isBlank()) {
                                        authProvider.profile.facebookProfileName = jsonObj.optString("first_name")
                                    }
//                                    if (authProvider.profile.lastName.isBlank()) {
//                                        authProvider.profile.lastName = jsonObj.optString("last_name")
//                                    }
//                                    if (authProvider.profile.email.isBlank()) {
//                                        authProvider.profile.email = jsonObj.optString("email")
//                                    }
                                    if (authProvider.profile.photo.isBlank()) {
                                        val picture = jsonObj.optJSONObject("picture")
                                        val data = picture?.optJSONObject("data")
                                        authProvider.profile.photo = data?.optString("url") ?: ""
                                    }
                                    credentialsSubject.onNext(FacebookAuthProvider.getCredential(result.accessToken.token))
                                } else {
                                    if (it is FirebaseAuthUserCollisionException) {
                                        errorsObserver.onNext(FacebookUserCollisionException())
                                    }else{
                                        errorsObserver.onNext(it)

                                    }
                                    processObserver.onNext(ProgressCompletedEvent())
                                }
                            }))
                        }else{
                            errorsObserver.onNext(response.error.exception)
                            processObserver.onNext(ProgressCompletedEvent())
                        }

                    }
                val parameters = Bundle()
                parameters.putString(
                    "fields",
                    "id,name,email,link,first_name,last_name, gender,birthday, picture.type(large)"
                )
                graphrequest.parameters = parameters
                graphrequest.executeAsync()
            }

        })
        LoginManager.getInstance().logInWithReadPermissions(fragment, listOf("public_profile", "email"))
    }
}