package com.digitalcard.app.ui.profile

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.digitalcard.app.R
import com.digitalcard.app.auth.UserProfile
import com.digitalcard.app.utils.RxBus
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.scanned_profile_item.view.*

class ScannedProfilesRecyclerAdapter(
    val ctx: Context,
    private val picasso: Picasso,
    private val rxBus: RxBus
) :
    RecyclerView.Adapter<ScannedProfilesRecyclerAdapter.ScannedProfileVH>() {
    private var lst  = mutableListOf<UserProfile>()
    private val layoutInflater = LayoutInflater.from(ctx)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScannedProfileVH {
        return ScannedProfileVH(
            ctx,
            layoutInflater.inflate(R.layout.scanned_profile_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return lst.size
    }

    override fun onBindViewHolder(holder: ScannedProfileVH, position: Int) {
        val item = lst[position]
        holder.bind(picasso, item, rxBus)
    }

    fun update(items: List<UserProfile>) {

        val diffs = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return lst[oldItemPosition].id == items[newItemPosition].id
            }

            override fun getOldListSize(): Int {
                return lst.size
            }

            override fun getNewListSize(): Int {
                return items.size
            }

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return lst[oldItemPosition].id == items[newItemPosition].id
            }
        })
        lst.clear()
        lst.addAll(items)
        diffs.dispatchUpdatesTo(this)
    }

    class ScannedProfileVH(val context: Context, itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(
            picasso: Picasso,
            item: UserProfile,
            rxBus: RxBus
        ) {

            if (!item.photo.isNullOrEmpty()){
                Glide.with(context).load(item.photo).into(itemView.userImage)
            } else {
                itemView.userImage.setImageResource(R.drawable.icon_male_ph)
            }

            itemView.userName.text = item.firstName

            itemView.setOnClickListener {
                rxBus.post(item)
            }
        }
    }
}