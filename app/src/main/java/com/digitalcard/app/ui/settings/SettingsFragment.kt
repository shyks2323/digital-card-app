package com.digitalcard.app.ui.settings

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.digitalcard.app.R
import com.digitalcard.app.auth.AuthProvider
import com.digitalcard.app.auth.UserProfile
import com.digitalcard.app.ui.BaseFragment
import com.digitalcard.app.ui.signin.SignInActivity
import com.digitalcard.app.utils.RxBus
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.mymaster.android.di.vm.ViewModelFactory
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_settings.*
import javax.inject.Inject


class SettingsFragment : BaseFragment() {
    @Inject
    lateinit var vmFactory: ViewModelFactory

    lateinit var vm: SettingsViewModel

    @Inject
    lateinit var rxBus: RxBus

    @Inject
    lateinit var authProvider: AuthProvider

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm = ViewModelProviders.of(this, vmFactory)[SettingsViewModel::class.java]

        val profile = authProvider.profile

        logoutButton.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            openSignInActivity()
        }

        deleteAccountButton.setOnClickListener {
            deleteAccount()
            openSignInActivity()
        }
        }

    private fun openSignInActivity() {
        val intent = Intent(requireActivity(), SignInActivity::class.java)
        startActivity(intent)
        requireActivity().finish()
    }

    private fun deleteAccount() {
        val userRef = FirebaseDatabase.getInstance().getReference("users/profiles/${authProvider.profile.id}")
        userRef.removeValue()
        authProvider.profile = UserProfile()
    }
}