package com.digitalcard.app.ui.settings

import com.digitalcard.app.api.ApiService
import com.digitalcard.app.ui.BaseViewModel
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject


class SettingsViewModel @Inject constructor(private val api: ApiService) : BaseViewModel() {


    fun load() {
        Timber.d("load")
    }

    private fun refreshData() {
    }
}