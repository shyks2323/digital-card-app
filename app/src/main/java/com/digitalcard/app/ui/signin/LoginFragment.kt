package com.digitalcard.app.ui.signin

import android.content.Context
import android.os.Bundle
import android.telephony.TelephonyManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import com.digitalcard.app.R
import com.digitalcard.app.auth.AuthProvider
import com.digitalcard.app.ui.BaseFragment
import com.digitalcard.app.utils.*
import com.jakewharton.rxbinding3.view.clicks
import com.mymaster.android.di.vm.ViewModelFactory
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_login.view.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class LoginFragment : BaseFragment() {

    @Inject
    lateinit var rxBus: RxBus
    @Inject
    lateinit var vmFactory: ViewModelFactory
    lateinit var vm: RegisterClientViewModel

    @Inject
    lateinit var locationProvider: LocationProvider

    @Inject
    lateinit var authProvider: AuthProvider

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm = ViewModelProviders.of(requireActivity(), vmFactory)[RegisterClientViewModel::class.java]


        val itemsCount = phoneNumbersGrid.childCount
        for (i in 0 until itemsCount){
            val itemView = phoneNumbersGrid.getChildAt(i)
            when (itemView){
                is Button -> {
                    itemView.setOnClickListener {
                        if (phoneNumberValue.text!!.length <= 13)
                            phoneNumberValue.append(itemView.text)
                    }
                }
                else -> {
                    itemView.setOnClickListener {
                        val currentText = phoneNumberValue.text.toString()
                        if (currentText.isNotEmpty()){
                            val result = currentText.substring(0, currentText.length-1)
                            phoneNumberValue.setText(result)
                        }
                    }
                }
            }
        }

        compositeDisposable.add(vm.errorsObserver.subscribe {
//            authProvider.profile.firstName = ""
//            authProvider.profile.lastName = ""
//            authProvider.profile.email = ""
//            authProvider.profile.phoneNumber = ""
        })

        compositeDisposable.add(vm.processObserver.observeOn(AndroidSchedulers.mainThread()).subscribe {
            when (it) {
                is ProgressStartedEvent -> {
                    deleteAccountButton.disableWithAlpha()
                }
                is ProgressCompletedEvent -> {
                    deleteAccountButton.enableWithAlpha()
                }
            }
        })

        compositeDisposable.add(view.deleteAccountButton.clicks().throttleFirst(1, TimeUnit.SECONDS).subscribe{
            val errorText = when {
                view.phoneNumberValue.text.isNullOrEmpty() -> resources.getString(R.string.phone_number_empty)
                else -> ""
            }

            if (errorText.isEmpty()) {
                val phone = view.phoneNumberValue.text.toString()
//                val phone = """${view.countryCode.text}${view.phoneNumberValue.text.toString()}"""
                authProvider.profile.phoneNumber = phone
                vm.phoneEntered(phone, true)
            }  else {
                AlertDialog.Builder(requireActivity()).apply {
                    setTitle(R.string.error_def_title)
                    setMessage(errorText)
                    setPositiveButton(
                        android.R.string.ok
                    ) { dialog, _ -> dialog.dismiss() }
                }.show()
            }
        })

        val man = requireActivity().getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager?
        if (man != null && !man.simCountryIso.isNullOrBlank()) {
            val index = CountryCodes.getCode(man.simCountryIso)
            phoneNumberValue.setText("+$index")
//            view.countryCode.text = "+$index"
        }
    }
}