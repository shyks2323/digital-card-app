package com.digitalcard.app.ui.signin

import android.app.Activity
import android.os.Bundle
import com.digitalcard.app.api.ApiService
import com.digitalcard.app.auth.AuthProvider
import com.digitalcard.app.auth.CodeSent
import com.digitalcard.app.ui.BaseViewModel
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.firebase.auth.*
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject


class ProgressStartedEvent
class ProgressCompletedEvent
class UserModeWrongError : Throwable()
class UserNotExistsError : Throwable()
class FacebookUserNotExistsError : Throwable()

class RegisterClientViewModel @Inject constructor(
    val authProvider: AuthProvider,
//    val picasso: Picasso,
    val apiService: ApiService
) : BaseViewModel() {
    private var codeSent: CodeSent? = null


    val processObserver = PublishSubject.create<Any>()
    val errorsObserver = PublishSubject.create<Throwable>()
    val credentialsSubject = PublishSubject.create<AuthCredential>()
    var isSignIn: Boolean = false

    fun codeEntered(code: String) {
        processObserver.onNext(ProgressStartedEvent())
        val credential = PhoneAuthProvider.getCredential(codeSent?.token ?: "", code)
        credentialsSubject.onNext(credential)
    }

    fun phoneEntered(phone: String, validate: Boolean) {
        processObserver.onNext(ProgressStartedEvent())
        if (validate) {
            compositeDisposable.add(
                apiService.checkUserPhone(phone).subscribe({
                    isSignIn = false
                    compositeDisposable.add(performLogin(phone))
                }, {
                    if (it is FirebaseAuthUserCollisionException){
                        isSignIn = true
                        compositeDisposable.add(performLogin(phone))
                    } else {
                        errorsObserver.onNext(it)
                        processObserver.onNext(ProgressCompletedEvent())
                    }
                })
            )

        } else {
            compositeDisposable.add(apiService.checkUserPhone(phone).subscribe({
                errorsObserver.onNext(UserNotExistsError())
                processObserver.onNext(ProgressCompletedEvent())
            }, {
                if (it is FirebaseAuthUserCollisionException){
                    isSignIn = true
                    compositeDisposable.add(performLogin(phone))
                } else {
                    errorsObserver.onNext(it)
                    processObserver.onNext(ProgressCompletedEvent())
                }

//                compositeDisposable.add(performLogin(phone))
            }))
        }

    }

    private fun performLogin(phone: String): Disposable {
        return authProvider.loginPhone(phone).observeOn(AndroidSchedulers.mainThread()).subscribe(
            {
                if (it is PhoneAuthCredential) {
                    //login completed
                    Timber.d("PhoneAuthCredential")
                    processObserver.onNext(ProgressCompletedEvent())
                    credentialsSubject.onNext(it)
                }
                if (it is CodeSent) {
                    Timber.d("CodeSent")
                    codeSent = it
                    processObserver.onNext(ProgressCompletedEvent())
                    processObserver.onNext(it)
                }
            },
            {
                errorsObserver.onNext(it)
                processObserver.onNext(ProgressCompletedEvent())
                Timber.e(it, "authFailed")
            })
    }

    fun loginIn(credentials: AuthCredential): Observable<FirebaseUser> {
        return Observable.create { emitter ->
            processObserver.onNext(ProgressStartedEvent())
            val disposable = authProvider.signIn(credentials).subscribe({ user ->
                authProvider.loadCurrentUser().subscribe({
                    authProvider.profile.id = authProvider.currentUser.uid()
                    emitter.onNext(authProvider.currentUser.user!!)
                }, {
                    emitter.onError(it)
                })
            }, {
                processObserver.onNext(ProgressCompletedEvent())
                if (!emitter.isDisposed) {
                    emitter.onError(it)
                }
            })
            emitter.setCancellable {
                disposable.dispose()
            }
        }
    }

    fun signIn(credentials: AuthCredential): Observable<FirebaseUser> {
        return Observable.create { emitter ->
            processObserver.onNext(ProgressStartedEvent())
            val disposable = authProvider.signIn(credentials).subscribe({ user ->
                createProfile(emitter, user)
            }, {
                processObserver.onNext(ProgressCompletedEvent())
                if (!emitter.isDisposed) {
                    emitter.onError(it)
                }
            })
            emitter.setCancellable {
                disposable.dispose()
            }
        }

    }

    fun signUp(it: AuthCredential): Observable<FirebaseUser> {
        return Observable.create { emitter ->
            processObserver.onNext(ProgressStartedEvent())
            val disposable = authProvider.signUp(it).subscribe({ user ->
                createProfile(emitter, user)
            }, {
                processObserver.onNext(ProgressCompletedEvent())
                if (!emitter.isDisposed) {
                    emitter.onError(it)
                }
            })
            emitter.setCancellable {
                disposable.dispose()
            }

        }
    }

    private fun createProfile(
        emitter: ObservableEmitter<FirebaseUser>,
        user: FirebaseUser
    ) {

        compositeDisposable.add(
            authProvider.createProfile().subscribe({
                authProvider.loadCurrentUser().subscribe({
                    processObserver.onNext(ProgressCompletedEvent())
                    if (!emitter.isDisposed) {
                        emitter.onNext(user)
                    }
                },{
                    Timber.e(it)
                    processObserver.onNext(ProgressCompletedEvent())
                    if (!emitter.isDisposed) {
                        emitter.onError(it)
                    }
                })
            }, {
                Timber.e(it)
                processObserver.onNext(ProgressCompletedEvent())
                if (!emitter.isDisposed) {
                    emitter.onError(it)
                }
            })
        )
    }

    class FacebookUserCollisionException:Exception()

    fun facebookStart(fragment: Activity, validate: Boolean) {
        processObserver.onNext(ProgressStartedEvent())
        LoginManager.getInstance().logOut()
        LoginManager.getInstance().registerCallback(authProvider.clbManager, object : FacebookCallback<LoginResult> {
            override fun onCancel() {
                Timber.d("onCancel")
            }

            override fun onError(error: FacebookException) {
                Timber.e(error, "onError")
                errorsObserver.onNext(error)
            }

            override fun onSuccess(result: LoginResult) {
                Timber.d("onSuccess")

                val graphrequest =
                    GraphRequest.newMeRequest(result.accessToken) { jsonObj, response ->

                        if (response.error == null){
                            compositeDisposable.add(apiService.checkFacebookId(jsonObj.optString("id")).subscribe({
                                if (validate) {
                                    errorsObserver.onNext(FacebookUserNotExistsError())
                                    processObserver.onNext(ProgressCompletedEvent())
                                } else {
                                    authProvider.profile.facebookId = jsonObj.optString("id")
                                    if (authProvider.profile.firstName.isBlank()) {
                                        authProvider.profile.firstName = jsonObj.optString("first_name")
                                    }
                                    if (authProvider.profile.lastName.isBlank()) {
                                        authProvider.profile.lastName = jsonObj.optString("last_name")
                                    }
                                    if (authProvider.profile.email.isBlank()) {
                                        authProvider.profile.email = jsonObj.optString("email")
                                    }
                                    if (authProvider.profile.photo.isBlank()) {
                                        val picture = jsonObj.optJSONObject("picture")
                                        val data = picture?.optJSONObject("data")
                                        authProvider.profile.photo = data?.optString("url") ?: ""
                                    }
                                    credentialsSubject.onNext(FacebookAuthProvider.getCredential(result.accessToken.token))
                                }
                            }, {
                                if (validate && it is FirebaseAuthUserCollisionException) {
                                    authProvider.profile.facebookId = jsonObj.optString("id")
                                    if (authProvider.profile.firstName.isBlank()) {
                                        authProvider.profile.firstName = jsonObj.optString("first_name")
                                    }
                                    if (authProvider.profile.lastName.isBlank()) {
                                        authProvider.profile.lastName = jsonObj.optString("last_name")
                                    }
                                    if (authProvider.profile.email.isBlank()) {
                                        authProvider.profile.email = jsonObj.optString("email")
                                    }
                                    if (authProvider.profile.photo.isBlank()) {
                                        val picture = jsonObj.optJSONObject("picture")
                                        val data = picture?.optJSONObject("data")
                                        authProvider.profile.photo = data?.optString("url") ?: ""
                                    }
                                    credentialsSubject.onNext(FacebookAuthProvider.getCredential(result.accessToken.token))
                                } else {
                                    if (it is FirebaseAuthUserCollisionException) {
                                        errorsObserver.onNext(FacebookUserCollisionException())
                                    }else{
                                        errorsObserver.onNext(it)

                                    }
                                    processObserver.onNext(ProgressCompletedEvent())
                                }
                            }))
                        }else{
                            errorsObserver.onNext(response.error.exception)
                            processObserver.onNext(ProgressCompletedEvent())
                        }

                    }
                val parameters = Bundle()
                parameters.putString(
                    "fields",
                    "id,name,email,first_name,last_name, gender,birthday, picture.type(large)"
                )
                graphrequest.parameters = parameters
                graphrequest.executeAsync()
            }

        })
        LoginManager.getInstance().logInWithReadPermissions(fragment, listOf("public_profile", "email"))
    }

//    private fun loadPhoto(uri: Uri): Single<String> {
//        return Single.create { emitter ->
//            val uid = authProvider.currentUser.uid()
//            picasso.load(uri).into(object : Target {
//                override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
//
//                }
//
//                override fun onBitmapFailed(e: Exception, errorDrawable: Drawable?) {
//                    Timber.e(e)
//                    emitter.onSuccess("")
//                }
//
//                override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom?) {
//                    val storagePath = "users/$uid/${UUID.randomUUID()}.png"
//                    val ref = FirebaseStorage.getInstance().getReference(storagePath)
//                    val metadata = StorageMetadata.Builder().build()
//                    val baos = ByteArrayOutputStream()
//                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
//                    val data = baos.toByteArray()
//                    ref.putBytes(data, metadata).addOnFailureListener {
//                        Timber.e(it)
//                        emitter.onSuccess("")
//                    }.addOnSuccessListener {
//                        emitter.onSuccess(storagePath)
//                    }
//                }
//
//            })
//        }
//    }
}