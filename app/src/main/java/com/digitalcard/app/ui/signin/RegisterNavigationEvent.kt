package com.digitalcard.app.ui.signin

import com.digitalcard.app.ui.HomeNavigationEvent

sealed class RegisterNavigationEvent {
    data class RegisterNavigateAction(val dest: Int, val login:Boolean)
    object ClientRegisterCompleteEvent : RegisterNavigationEvent()
}