package com.digitalcard.app.ui.signin

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.NavHost
import androidx.navigation.findNavController
import com.digitalcard.app.R
import com.digitalcard.app.api.ApiService
import com.digitalcard.app.auth.AuthProvider
import com.digitalcard.app.auth.CodeSent
import com.digitalcard.app.ui.BaseActivity
import com.digitalcard.app.ui.HomeNavigation
import com.digitalcard.app.ui.HomeNavigationEvent
import com.digitalcard.app.utils.RxBus
import com.facebook.FacebookAuthorizationException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.mymaster.android.di.vm.ViewModelFactory
import io.reactivex.android.schedulers.AndroidSchedulers
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class SignInActivity : BaseActivity(), NavHost {

    @Inject
    lateinit var rxBus: RxBus

    @Inject
    lateinit var vmFactory: ViewModelFactory
    lateinit var vm: RegisterClientViewModel

    @Inject
    lateinit var api: ApiService

    private var signIn: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        vm = ViewModelProviders.of(this, vmFactory)[RegisterClientViewModel::class.java]
        FirebaseAuth.getInstance().setLanguageCode(Locale.getDefault().language)

        compositeDisposable.add(rxBus.events.observeOn(AndroidSchedulers.mainThread()).subscribe {
            if (it is RegisterNavigationEvent.RegisterNavigateAction) {
                val opts = Bundle()
                opts.putBoolean("signin", it.login)
                signIn = it.login
                vm.isSignIn = it.login
                navController.navigate(it.dest, opts)
            }
            if (it is RegisterNavigationEvent.ClientRegisterCompleteEvent) {
                val intent = Intent(this@SignInActivity, HomeNavigation::class.java)
                startActivity(intent)
                finish()
            }
        })

        compositeDisposable.add(vm.processObserver.observeOn(AndroidSchedulers.mainThread()).subscribe {
            if (it is CodeSent) {
                navController.navigate(R.id.action_loginFragment_to_verificationFragment)
            }
        })

        compositeDisposable.add(vm.errorsObserver.observeOn(AndroidSchedulers.mainThread()).subscribe {
            handleAuthError(null, it)
        })


        compositeDisposable.add(vm.credentialsSubject.subscribe({
            loginCompleted(it)
        }, {
            Timber.e(it)
        }))
    }

    override fun getNavController(): NavController {
        return findNavController(R.id.register_client_navhost)
    }


    private fun handleAuthError(credentials: AuthCredential?, it: Throwable) {
        Timber.e(it,"handleAuthError")
        when (it) {

            is FirebaseAuthUserCollisionException -> {
                AlertDialog.Builder(this).apply {
                    setTitle(R.string.error_def_title)
                    setMessage(R.string.error_firebase_user_already_exists)
                    setPositiveButton(
                        android.R.string.ok
                    ) { dialog, _ -> dialog.dismiss() }
                }.show()
            }

            is FirebaseTooManyRequestsException -> {
                AlertDialog.Builder(this).apply {
                    setTitle(R.string.error_def_title)
                    setMessage(R.string.error_firebase_too_many_requests)
                    setPositiveButton(
                        android.R.string.ok
                    ) { dialog, _ -> dialog.dismiss() }
                }.show()
            }
            is FirebaseAuthInvalidCredentialsException -> {
                when (it.errorCode) {
                    "ERROR_INVALID_VERIFICATION_CODE" -> {
                        AlertDialog.Builder(this).apply {
                            setTitle(R.string.error_def_title)
                            setMessage(R.string.error_firebase_code_invalid)
                            setPositiveButton(
                                android.R.string.ok
                            ) { dialog, _ -> dialog.dismiss() }
                        }.show()
                    }
                    "ERROR_INVALID_PHONE_NUMBER" -> {
                        AlertDialog.Builder(this).apply {
                            setTitle(R.string.error_def_title)
                            setMessage(R.string.error_firebase_number_invalid)
                            setPositiveButton(
                                android.R.string.ok
                            ) { dialog, _ -> dialog.dismiss() }
                        }.show()
                    }
                    else -> {
                        AlertDialog.Builder(this).apply {
                            setTitle(R.string.error_def_title)
                            setMessage(R.string.error_unknown)
                            setPositiveButton(
                                android.R.string.ok
                            ) { dialog, _ -> dialog.dismiss() }
                        }.show()
                    }

                }
            }
            else -> {
                AlertDialog.Builder(this).apply {
                    setTitle(R.string.error_def_title)
                    setMessage(R.string.error_unknown)
                    setPositiveButton(
                        android.R.string.ok
                    ) { dialog, _ -> dialog.dismiss() }
                }.show()
            }
        }
    }

    private fun loginCompleted(credential: AuthCredential) {
        Timber.d("loginCompleted")
        if (vm.isSignIn){
            compositeDisposable.add(vm.loginIn(credential).subscribe({
                rxBus.post(RegisterNavigationEvent.ClientRegisterCompleteEvent)
            }, {
                handleAuthError(credential, it)
                Timber.e(it)
            }))
        } else {
            compositeDisposable.add(vm.signUp(credential).subscribe({
                rxBus.post(RegisterNavigationEvent.ClientRegisterCompleteEvent)
            }, {
                handleAuthError(credential, it)
                Timber.e(it)
            }))
        }

    }

}