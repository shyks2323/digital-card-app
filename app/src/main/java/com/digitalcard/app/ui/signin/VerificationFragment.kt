package com.digitalcard.app.ui.signin

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProviders
import com.digitalcard.app.R
import com.digitalcard.app.ui.BaseFragment
import com.digitalcard.app.utils.RxBus
import com.digitalcard.app.utils.disableWithAlpha
import com.digitalcard.app.utils.enableWithAlpha
import com.jakewharton.rxbinding3.view.clicks
import com.mymaster.android.di.vm.ViewModelFactory
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_verification.*
import kotlinx.android.synthetic.main.fragment_verification.view.*
import java.lang.StringBuilder
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class VerificationFragment : BaseFragment() {

    @Inject
    lateinit var rxBus: RxBus
    @Inject
    lateinit var vmFactory: ViewModelFactory
    lateinit var vm: RegisterClientViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? { // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_verification, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm = ViewModelProviders.of(requireActivity(), vmFactory)[RegisterClientViewModel::class.java]
        compositeDisposable.add(vm.processObserver.observeOn(AndroidSchedulers.mainThread()).subscribe {
            when (it) {
                is ProgressStartedEvent -> {
                    view.confirmButton.disableWithAlpha()
                }
                is ProgressCompletedEvent -> {
                    view.confirmButton.enableWithAlpha()
                }
            }
        })

        compositeDisposable.add(vm.credentialsSubject.observeOn(AndroidSchedulers.mainThread()).subscribe({

        }, {

        }))
        compositeDisposable.add(view.confirmButton.clicks().throttleFirst(1, TimeUnit.SECONDS).subscribe {
            val phone = provideVerificationCode()
            if (phone.length == 6){
                vm.codeEntered(phone)
            } else {
                AlertDialog.Builder(requireActivity()).apply {
                    setTitle(R.string.error_def_title)
                    setMessage(R.string.code_length_invalid)
                    setPositiveButton(
                        android.R.string.ok
                    ) { dialog, _ -> dialog.dismiss() }
                }.show()
            }
        })


        val itemsCount = phoneNumbersGrid.childCount
        for (i in 0 until itemsCount){
            val itemView = phoneNumbersGrid.getChildAt(i)
            when (itemView){
                is Button -> {
                    itemView.setOnClickListener {
                        when {
                            firstNumber.text.isNullOrEmpty() -> {
                                firstNumber.text = itemView.text
                            }
                            secondNumber.text.isNullOrEmpty() -> {
                                secondNumber.text = itemView.text
                            }
                            thirdNumber.text.isNullOrEmpty() -> {
                                thirdNumber.text = itemView.text
                            }
                            fourthNumber.text.isNullOrEmpty() -> {
                                fourthNumber.text = itemView.text
                            }
                            fifthNumber.text.isNullOrEmpty() -> {
                                fifthNumber.text = itemView.text
                            }
                            sixthNumber.text.isNullOrEmpty() -> {
                                sixthNumber.text = itemView.text
//                                progressBar.visibility = View.VISIBLE
//                                signInWithPhone(provideVerificationCode())
                            }
                        }
                    }
                }
                else -> {
                    itemView.setOnClickListener {
                        when {
                            sixthNumber.text.isNotEmpty() -> {
                                sixthNumber.text = ""
                            }
                            fifthNumber.text.isNotEmpty() -> {
                                fifthNumber.text = ""
                            }
                            fourthNumber.text.isNotEmpty() -> {
                                fourthNumber.text = ""
                            }
                            thirdNumber.text.isNotEmpty() -> {
                                thirdNumber.text = ""
                            }
                            secondNumber.text.isNotEmpty() -> {
                                secondNumber.text = ""
                            }
                            firstNumber.text.isNotEmpty() -> {
                                firstNumber.text = ""
                            }
                        }
                    }
                }
            }
        }
    }

    private fun provideVerificationCode(): String {
        val stringBuilder = StringBuilder()
        stringBuilder.append(firstNumber.text)
        stringBuilder.append(secondNumber.text)
        stringBuilder.append(thirdNumber.text)
        stringBuilder.append(fourthNumber.text)
        stringBuilder.append(fifthNumber.text)
        stringBuilder.append(sixthNumber.text)

        return stringBuilder.toString()
    }
}