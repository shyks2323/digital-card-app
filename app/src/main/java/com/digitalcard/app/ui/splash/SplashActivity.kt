package com.digitalcard.app.ui.splash

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.digitalcard.app.R
import com.digitalcard.app.api.ApiService
import com.digitalcard.app.auth.AuthProvider
import com.digitalcard.app.ui.BaseActivity
import com.digitalcard.app.ui.HomeNavigation
import com.digitalcard.app.ui.signin.SignInActivity
import com.digitalcard.app.utils.LocationProvider
import com.google.firebase.auth.FirebaseAuth
import com.mymaster.android.di.vm.ViewModelFactory
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SplashActivity : BaseActivity() {

    @Inject
    lateinit var vmFactory: ViewModelFactory

    @Inject
    lateinit var locationProvider: LocationProvider

    @Inject
    lateinit var authProvider: AuthProvider

    @Inject
    lateinit var api: ApiService

    lateinit var vm: SplashViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        vm = ViewModelProviders.of(this, vmFactory)[SplashViewModel::class.java]
//
//        FirebaseAuth.getInstance().signOut()

        compositeDisposable.add(vm.dataObservable.observeOn(Schedulers.newThread()).subscribe {
            Timber.d("Finish splash")
            if (it.isAnonymous()) {
//                findNavController(R.navigation.nav_graph).popBackStack()
//                findNavController(R.navigation.nav_graph).navigate(R.id.home)
                startActivity(Intent(this@SplashActivity, SignInActivity::class.java))
                finish()
            } else {
                startActivity(Intent(this@SplashActivity, HomeNavigation::class.java))
                finish()
            }
        })

        vm.load()
//        compositeDisposable.add(vm.load().subscribe {
//            Timber.d("Finish splash")
//            startActivity(Intent(this@SplashActivity, HomeNavigation::class.java))
//            finish()
//        })
    }
}
