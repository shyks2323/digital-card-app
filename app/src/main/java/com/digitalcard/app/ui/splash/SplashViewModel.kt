package com.digitalcard.app.ui.splash

import com.digitalcard.app.api.ApiService
import com.digitalcard.app.auth.AuthProvider
import com.digitalcard.app.auth.UserHolder
import com.digitalcard.app.ui.BaseViewModel
import com.digitalcard.app.utils.LocationProvider
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class SplashViewModel @Inject constructor(
    val authProvider: AuthProvider,
    val apiService: ApiService,
    val locationProvider: LocationProvider
) : BaseViewModel() {

    val dataObservable = PublishSubject.create<UserHolder>()

    fun load() {
        compositeDisposable.add((userHolder()).subscribe({
            dataObservable.onNext(it)
        },{
            dataObservable.onError(it)
        }))
    }

    private fun userHolder(): Observable<UserHolder> {
        return authProvider.loadCurrentUser().toObservable()
    }
}