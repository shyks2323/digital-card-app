package com.digitalcard.app.utils

import android.annotation.SuppressLint
import android.content.Context
import android.location.Address
import android.location.Location
import android.location.LocationManager
import android.os.Looper
import com.google.android.gms.location.*
import com.google.android.gms.maps.LocationSource
import com.google.android.gms.maps.model.LatLng
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber
import java.util.concurrent.TimeUnit


class LocationProvider(val ctx: Context) : LocationSource {
    override fun deactivate() {
        Timber.d("LocationSource: deactivate")
        locationSourceDisposable?.dispose()
    }

    override fun activate(p0: LocationSource.OnLocationChangedListener?) {
        Timber.d("LocationSource: activate")
        locationSourceDisposable = locationEvents.subscribe {
            Timber.d("LocationSource: onLocationChanged")
            p0?.onLocationChanged(it)
        }
    }

    private var locationSourceDisposable: Disposable? = null


    val locationEvents = BehaviorSubject.create<Location>()
    private val fusedLocationClient = LocationServices.getFusedLocationProviderClient(ctx)

    @SuppressLint("MissingPermission")
    fun query() {

        fusedLocationClient.lastLocation.addOnSuccessListener {
            locationEvents.onNext(it)
        }
    }

    fun updateGlobal(location: Location) {
        if (isBetterLocation(location, lastKnown)) {
            lastKnown = location
            locationEvents.onNext(location)
        }
    }

    var lastKnown: Location = Location("").also {
        it.longitude = 0.0
        it.latitude = 0.0
    }
    private val locationSubscribers = mutableMapOf<String, LocationCallback>()
    @SuppressLint("MissingPermission")
    fun subscribe(): Observable<Location> {
        return Observable.create { emitter ->
            Timber.d("rxSubscribed")
            val request = LocationRequest.create().apply {
                fastestInterval = TimeUnit.SECONDS.toMillis(5)
                interval = TimeUnit.SECONDS.toMillis(10)
            }
            val clb = object : LocationCallback() {
                var bestKnown: Location? = null
                override fun onLocationResult(locationResult: LocationResult?) {
                    super.onLocationResult(locationResult)
                    locationResult ?: return
                    var best = locationResult.lastLocation
                    if (best != null) {
                        for (location in locationResult.locations) {
                            if (isBetterLocation(location, best)) {
                                best = location
                            }
                        }
                        if (isBetterLocation(best, bestKnown)) {
                            bestKnown = best
                            Timber.d("rxUpdate location")
                            emitter.onNext(bestKnown!!)
                            updateGlobal(bestKnown!!)
                        }
                    }


                }

                override fun onLocationAvailability(p0: LocationAvailability?) {
                    super.onLocationAvailability(p0)
                }
            }
            fusedLocationClient.requestLocationUpdates(request, clb, Looper.getMainLooper())
            emitter.setCancellable {
                Timber.d("rxUnSubscribed")
                fusedLocationClient.removeLocationUpdates(clb)
            }
            fusedLocationClient.lastLocation.addOnSuccessListener {
                locationEvents.onNext(it)
            }
        }
    }

    @SuppressLint("MissingPermission")
    fun subscribe(tag: String) {
        val callback = locationSubscribers[tag]
        if (callback == null) {
            val request = LocationRequest.create().apply {
                fastestInterval = TimeUnit.SECONDS.toMillis(5)
                interval = TimeUnit.SECONDS.toMillis(10)
            }
            val clb = object : LocationCallback() {
                override fun onLocationResult(p0: LocationResult?) {
                    super.onLocationResult(p0)

                }

                override fun onLocationAvailability(p0: LocationAvailability?) {
                    super.onLocationAvailability(p0)
                }
            }
            fusedLocationClient.requestLocationUpdates(request, clb, Looper.getMainLooper())
            locationSubscribers[tag] = clb
        } else {
            Timber.d("Already subscribed for tag: $tag")
        }
    }

    fun unsubscribe(tag: String) {
        val callback = locationSubscribers[tag]
        if (callback != null) {
            fusedLocationClient.removeLocationUpdates(callback)
            locationSubscribers.remove(tag)
            Timber.d("UnSubscribed for tag: $tag")
        } else {
            Timber.d("Not subscribed for tag: $tag")
        }
    }

    companion object {
        private const val TWO_MINUTES: Long = 1000 * 60 * 2
    }

    fun isBetterLocation(location: Location, currentBestLocation: Location?): Boolean {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true
        }

        // Check whether the new location fix is newer or older
        val timeDelta: Long = location.time - currentBestLocation.time
        val isSignificantlyNewer: Boolean = timeDelta > TWO_MINUTES
        val isSignificantlyOlder: Boolean = timeDelta < -TWO_MINUTES

        when {
            // If it's been more than two minutes since the current location, use the new location
            // because the user has likely moved
            isSignificantlyNewer -> return true
            // If the new location is more than two minutes older, it must be worse
            isSignificantlyOlder -> return false
        }

        // Check whether the new location fix is more or less accurate
        val isNewer: Boolean = timeDelta > 0L
        val accuracyDelta: Float = location.accuracy - currentBestLocation.accuracy
        val isLessAccurate: Boolean = accuracyDelta > 0f
        val isMoreAccurate: Boolean = accuracyDelta < 0f
        val isSignificantlyLessAccurate: Boolean = accuracyDelta > 200f

        // Check if the old and new location are from the same provider
        val isFromSameProvider: Boolean = location.provider == currentBestLocation.provider

        // Determine location quality using a combination of timeliness and accuracy
        return when {
            isMoreAccurate -> true
            isNewer && !isLessAccurate -> true
            isNewer && !isSignificantlyLessAccurate && isFromSameProvider -> true
            else -> false
        }
    }

    fun distanceTo(location: LatLng): Int {
        val res = Location("").also {
            it.longitude = location.longitude
            it.latitude = location.latitude
        }

        return (lastKnown?.distanceTo(res) ?: 0.0f).toInt()
    }

    @SuppressLint("MissingPermission")
    private fun compatLastOnce():Observable<Location>{
        return Observable.create { emitter ->
            val locManager = ctx.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            locManager.allProviders.forEach { provider->
                val loc = locManager.getLastKnownLocation(provider)
                if (loc!=null){
                    emitter.onNext(loc)
                }
            }
            emitter.onComplete()

        }
    }
    @SuppressLint("MissingPermission")
    private fun internalLastOnce():Observable<Location>{
        return Observable.create { emitter ->
            fusedLocationClient.lastLocation.addOnSuccessListener {
                if (it!=null) {
                    emitter.onNext(it)
                }
                if (lastKnown.latitude != 0.0 && lastKnown.longitude != 0.0){
                    emitter.onNext(lastKnown)
                }
                emitter.onComplete()

            }
        }
    }
    @SuppressLint("MissingPermission")
    fun queryOnce(): Observable<Location> {
        return Observable.mergeDelayError(listOf(compatLastOnce(),internalLastOnce(),internalRequestOnce())).take(1)
    }

    @SuppressLint("MissingPermission")
    private fun internalRequestOnce(): Observable<Location> {
        return Observable.create { emitter ->

            val request = LocationRequest.create().apply {
                fastestInterval = TimeUnit.SECONDS.toMillis(1)
                interval = TimeUnit.SECONDS.toMillis(1)
            }
            val clb = object : LocationCallback() {
                override fun onLocationResult(p0: LocationResult) {
                    super.onLocationResult(p0)
                    emitter.onNext(p0.lastLocation)
                    emitter.onComplete()
                    fusedLocationClient.removeLocationUpdates(this)

                }

                override fun onLocationAvailability(p0: LocationAvailability?) {
                    super.onLocationAvailability(p0)
                }
            }
            fusedLocationClient.requestLocationUpdates(request, clb, Looper.getMainLooper())
        }
    }

    fun distanceTo(location: Address?): Int {
        val latlng = if (location == null) {
            LatLng(0.0, 0.0)
        } else {
            LatLng(location.latitude, location.longitude)
        }
        return distanceTo(latlng)
    }
}