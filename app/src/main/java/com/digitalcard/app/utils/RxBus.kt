package com.digitalcard.app.utils

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class RxBus {

    private val subject = PublishSubject.create<Any>()

    /**
     * Subscribe to this Observable. On event, do something
     * e.g. replace a fragment
     */
    val events: Observable<Any>
        get() = subject

    /**
     * Pass any event down to event listeners.
     */
    fun post(obj: Any) {
        subject.onNext(obj)
    }
}