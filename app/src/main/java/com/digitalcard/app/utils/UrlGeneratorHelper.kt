package com.digitalcard.app.utils


class UrlGeneratorHelper {
    companion object {
        fun getUrl(from:String):String{
            if (from.isNullOrEmpty()){
                return "gs://firebase/"
            }
            if (!from.startsWith("http")){
                return "gs://firebase/$from"
            }
            return from
        }
    }
}