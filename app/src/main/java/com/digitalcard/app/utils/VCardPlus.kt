package com.digitalcard.app.utils

import com.digitalcard.app.auth.UserProfile
import net.glxn.qrgen.core.scheme.Schema
import net.glxn.qrgen.core.scheme.SchemeUtil
import net.glxn.qrgen.core.scheme.VCard

class VCardPlus private constructor(
    val name: String? = null,
    val phoneNumber: String? = null,
    val email: String? = null,
    val address: String? = null,
    val photo: String? = null,
    val website: String? = null,
    val note: String? = null,
    val skype: String? = null,
    val facebook: String? = null,
    val twitter: String? = null,
    var whatsApp: String? = null,
    val instagram: String? = null
): Schema() {
    private val LINE_SEPARATOR = "\n"
    private val SEPARATOR = "_"
    private val BEGIN_VCARD = "BEGIN;VCARD"
    private val NAME = "N"
    private val PHONE = "TEL"
    private val PHOTO = "PHOTO"
    private val WEB = "URL"
    private val EMAIL = "EMAIL"
    private val ADDRESS = "ADR"
    private val NOTE = "NOTE"
    private val XSKYPE = "Skype"
    private val FACEBOOK = "Facebook"
    private val TWITTER = "Twitter"
    private val WHATSAPP = "WhatsApp"
    private val INSTAGRAM = "Instagram"

    data class Builder(
        var name: String? = null,
        var phoneNumber: String? = null,
        var email: String? = null,
        var address: String? = null,
        var photo: String? = null,
        var website: String? = null,
        var note: String? = null,
        var xskype: String? = null,
        var facebook: String? = null,
        var twitter: String? = null,
        var whatsApp: String? = null,
        var instagram: String? = null
    ) {
        fun name (name: String?) = apply { this.name = name }
        fun phoneNumber (phoneNumber: String?) = apply { this.phoneNumber = phoneNumber }
        fun email (email: String?) = apply { this.email = email }
        fun skype (xskype: String?) = apply { this.xskype = xskype }
        fun facebook (facebookId: String?) = apply { this.facebook = facebookId }
        fun twitter (twitterId: String?) = apply { this.twitter = twitterId }
        fun whatsApp (whatsAppId: String?) = apply { this.whatsApp = whatsAppId }
        fun instagram (instagramId: String?) = apply { this.instagram = instagramId }

        fun address (address: String?) = apply { this.address = address }
        fun website (website: String?) = apply { this.website = website }
        fun note (note: String?) = apply { this.note = note }
        fun photo (photo: String?) = apply { this.photo = photo }


        fun build() = VCardPlus(name, phoneNumber, email, address, photo, website, note, xskype, facebook, twitter, whatsApp, instagram)
    }

    override fun generateString(): String? {
        val sb = StringBuilder()
        sb.append(BEGIN_VCARD)
        sb.append("VERSION;3.0")
        if (name != null) {
            sb.append(SEPARATOR).append(NAME).append(";").append(name)
        }
        if (email != null) {
            sb.append(SEPARATOR).append(EMAIL).append(";").append(email)
        }
        if (phoneNumber != null) {
            sb.append(SEPARATOR).append(PHONE).append(";").append(phoneNumber)
        }
        if (photo != null) {
            sb.append(SEPARATOR).append(PHOTO).append(";").append(photo)
        }

        if (facebook != null) {
            sb.append(SEPARATOR).append(FACEBOOK).append(";").append(facebook)
        }
        if (twitter != null) {
            sb.append(SEPARATOR).append(TWITTER).append(";").append(twitter)
        }
        if (whatsApp != null) {
            sb.append(SEPARATOR).append(WHATSAPP).append(";").append(whatsApp)
        }

        if (instagram != null) {
            sb.append(SEPARATOR).append(INSTAGRAM).append(";").append(instagram)
        }

        if (website != null) {
            sb.append(SEPARATOR).append(WEB).append(";").append(website)
        }
        if (address != null) {
            sb.append(SEPARATOR).append(ADDRESS).append(";").append(address)
        }
        if (note != null) {
            sb.append(SEPARATOR).append(NOTE).append(";").append(note)
        }
        sb.append(SEPARATOR).append("END;VCARD")

        return sb.toString()
    }

    fun toReadableFormatString(): String? {
        val sb = StringBuilder()
        if (name != null) {
            sb.append("Name").append(":").append(name)
        }
        if (email != null) {
            sb.append(LINE_SEPARATOR).append("Email").append(":").append(email)
        }
        if (phoneNumber != null) {
            sb.append(LINE_SEPARATOR).append("Phone number").append(":").append(phoneNumber)
        }
        if (photo != null) {
            sb.append(LINE_SEPARATOR).append("Photo").append(":").append(photo)
        }

        if (facebook != null) {
            sb.append(LINE_SEPARATOR).append("Facebook").append(":").append(facebook)
        }
        if (twitter != null) {
            sb.append(LINE_SEPARATOR).append("Twitter").append(":").append(twitter)
        }
        if (whatsApp != null) {
            sb.append(LINE_SEPARATOR).append("WhatsApp").append(":").append(whatsApp)
        }

        if (instagram != null) {
            sb.append(LINE_SEPARATOR).append("Instagram").append(":").append(instagram)
        }

        if (website != null) {
            sb.append(LINE_SEPARATOR).append("Website").append(":").append(website)
        }
        if (address != null) {
            sb.append(LINE_SEPARATOR).append("Address").append(":").append(address)
        }
        if (note != null) {
            sb.append(LINE_SEPARATOR).append("Note").append(":").append(note)
        }

        return sb.toString()
    }

    override fun parseSchema(code: String?): VCardPlus? {
        val parameters =
            SchemeUtil.getParameters(code, "_", ";")

        val builder = Builder()

        if (parameters.containsKey(NAME)) {
            builder.name = (parameters[NAME])
        }
        if (parameters.containsKey(PHONE)) {
            builder.phoneNumber = (parameters[PHONE])
        }
        if (parameters.containsKey(PHOTO)) {
            builder.photo = (parameters[PHOTO])
        }
        if (parameters.containsKey(EMAIL)) {
            builder.email = (parameters[EMAIL])
        }
        if (parameters.containsKey(FACEBOOK)) {
            builder.facebook = (parameters[FACEBOOK])
        }
        if (parameters.containsKey(TWITTER)) {
            builder.twitter = (parameters[TWITTER])
        }

        if (parameters.containsKey(WHATSAPP)) {
            builder.whatsApp = (parameters[WHATSAPP])
        }
        if (parameters.containsKey(INSTAGRAM)) {
            builder.instagram = (parameters[INSTAGRAM])
        }


        if (parameters.containsKey(ADDRESS)) {
            builder.address = (parameters[ADDRESS])
        }
        if (parameters.containsKey(WEB)) {
            builder.website = (parameters[WEB])
        }
        if (parameters.containsKey(NOTE)) {
            builder.note = (parameters[NOTE])
        }
        return builder.build()
    }

    /**
     * Returns the textual representation of this vcard of the form
     *
     *
     * BEGIN:VCARD N:John Doe ORG:Company TITLE:Title TEL:1234 URL:www.example.org
     * EMAIL:john.doe@example.org ADR:Street END:VCARD
     *
     */
    fun toEncodedString(): String? {
        return generateString()
    }

    fun parse(code: String?): VCardPlus? {
        val card = VCardPlus()
        card.parseSchema(code)
        return card
    }

    fun toUserProfile(): UserProfile {
        val userProfile = UserProfile()
        name?.let { userProfile.firstName = it }
        phoneNumber?.let { userProfile.phoneNumber = it }
        email?.let { userProfile.email = it }
        facebook?.let { userProfile.facebookId = it }
        twitter?.let { userProfile.twitterId = it }
        whatsApp?.let { userProfile.whatsappId = it }
        instagram?.let { userProfile.instagramId = it }
        photo?.let { userProfile.photo = it }

        return userProfile
    }
}