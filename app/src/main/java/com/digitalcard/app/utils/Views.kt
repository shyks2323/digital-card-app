package com.digitalcard.app.utils

import android.location.Address
import android.location.Location
import android.view.View
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import java.util.*

fun View.visible(value:Boolean){
    visibility = if (value){
        View.VISIBLE
    }else{
        View.GONE
    }
}
fun View.disableWithAlpha() {
    isEnabled = false
    animate().alpha(0.8f).start()
}

fun View.enableWithAlpha() {
    isEnabled = true
    animate().alpha(1f).start()
}

fun <T> List<T>.containsAny(col: Collection<T>): Boolean {
    col.forEach {
        if (contains(it)) {
            return true
        }
    }
    return false
}

fun <T> Set<T>.containsAny(col: Collection<T>): Boolean {
    col.forEach {
        if (contains(it)) {
            return true
        }
    }
    return false
}

fun Calendar.jodaFirstDayOfWeek(): Int {
    return ((firstDayOfWeek + 5) % 7) + 1
}

//fun DateTime.calendarWeekDay():Int{
//    return dayOfWeek+1
//}
//
//fun LocalDate.calendarWeekDay():Int{
//    return dayOfWeek+1
//}

fun Location.distanceTo(location: Address?): Float {
    val loc = if (location == null) {
        Location("").apply {
            longitude = 0.0
            latitude = 0.0
        }
    } else {
        Location("").apply {
            longitude = location.longitude
            latitude = location.latitude
        }
    }
    return distanceTo(loc)
}

fun MarkerOptions.position(location: Address?): MarkerOptions {
    if (location != null) {
        position(LatLng(location.latitude, location.longitude))
    } else {
        position(LatLng(0.0, 0.0))
    }
    return this
}
//
//fun Address.wrap(separator: String = ""): AddressWrapper {
//    val addressFragments = with(this) {
//        (0..maxAddressLineIndex).map { getAddressLine(it) }
//    }
//    return AddressWrapper(longitude, latitude, addressFragments.joinToString(separator))
//}
//
//fun Place.wrap(): AddressWrapper {
//
//    return AddressWrapper(latLng?.longitude ?: 0.0, latLng?.latitude ?: 0.0, this.address ?: "")
//}