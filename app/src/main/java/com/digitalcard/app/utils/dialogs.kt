package com.digitalcard.app.utils

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.bumptech.glide.Glide
import com.digitalcard.app.R
import com.digitalcard.app.api.data.AddContactEvent
import com.digitalcard.app.auth.UserProfile
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.add_contact_dialog.view.*
import org.jetbrains.anko.layoutInflater


/**
 * Create a dialog asking user if they enjoy the app
 *
 * @param callback used to handle events when user pressed buttons.
 */
fun createAddContactDialog(
    context: Context,
    userProfile: UserProfile,
    callback: (selectedValue: AddContactEvent?) -> Unit
): Dialog {
    var dialog: AlertDialog? = null
    val builder = MaterialAlertDialogBuilder(context, R.style.AddContactDialogTheme)
    val view = context.layoutInflater.inflate(R.layout.add_contact_dialog, null)


    if (userProfile.firstName.isNotEmpty()){
        view.userName.text = userProfile.firstName
    } else {
        view.userName.text = context.getString(R.string.unknown_profile)
    }

    if (userProfile.phoneNumber.isNotEmpty()){
        view.phone_number.text = userProfile.phoneNumber
    } else {
        view.phoneLayout.visibility = View.GONE
    }

    if (userProfile.facebookId.isNotEmpty()){
        view.facebookAccount.text = userProfile.facebookId
    } else {
        view.facebookLayout.visibility = View.GONE
    }

    if (userProfile.email.isNotEmpty()){
        view.gmailAccount.text = userProfile.email
    } else {
        view.gmailLayout.visibility = View.GONE
    }

    if (userProfile.twitterId.isNotEmpty()){
        view.twitterAccount.text = userProfile.twitterId
    } else {
        view.twitterLayout.visibility = View.GONE
    }

    if (userProfile.whatsappId.isNotEmpty()){
        view.whatsAppAccount.text = userProfile.whatsappId
    } else {
        view.whatsAppLayout.visibility = View.GONE
    }

    if (userProfile.instagramId.isNotEmpty()){
        view.instagramAccount.text = userProfile.instagramId
    } else {
        view.instagramLayout.visibility = View.GONE
    }

    if (userProfile.photo.isNotEmpty()){
        Glide.with(context).load(userProfile.photo).into(view.userImage)
    }

    view.addCardButton.setOnClickListener {
        callback(AddContactEvent.AddContactButtonPressed(userProfile))
//        dialog?.dismiss()
    }
    builder.setView(view)

    dialog = builder.create()

    return dialog
}