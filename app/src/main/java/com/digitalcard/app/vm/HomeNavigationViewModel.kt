package com.digitalcard.app.vm

import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class HomeNavigationViewModel @Inject constructor() {
    val unreadSubject = BehaviorSubject.create<Boolean>()
}